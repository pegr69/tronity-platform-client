# tronity-platform-client

[![pipeline status](https://gitlab.com/tronity/tronity-platform-client/badges/master/pipeline.svg)](https://gitlab.com/tronity/tronity-platform-client/-/commits/master)
[![coverage report](https://gitlab.com/tronity/tronity-platform-client/badges/master/coverage.svg)](https://gitlab.com/tronity/tronity-platform-client/-/commits/master)
[![License](https://img.shields.io/badge/license-MIT-yellow.svg)](https://img.shields.io/badge/license-MIT-yellow.svg)

## Description

A Python Package to easily access the Tronity Platform API.
Get data related to your vehicle like odometer, charge, battery
and location. Via the client you can conveniently get data from
the API and don't have to worry about recreating the token.

## Requirements

* python3

## Installation

```
pip3 install tronity-platform-client
```

## Getting Started
```python
from __future__ import print_function

import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    authenticationApi = tronity_platform_client.AuthenticationApi(api_client)
    vehiclesApi = tronity_platform_client.VehiclesApi(api_client)

    body = tronity_platform_client.InlineObject()
    body.client_id = "xxx"
    body.client_secret = "xxx"
    body.grant_type = "app"

    try:
        # Token which is valid for the app
        api_response = authenticationApi.auth_controller_authentication(body)
        configuration.access_token = api_response.access_token

        vehicles = vehiclesApi.get_many_base_vehicle_controller_vehicle()
        pprint(vehiclesApi.vehicle_controller_odometer(vehicles.data[0].id))
    except ApiException as e:
        print("Exception when calling AuthenticationApi->auth_controller_authentication: %s\n" % e)
    
```

## Documentation for API Endpoints

All URIs are relative to *https://api.eu.tronity.io*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AuthenticationApi* | [**auth_controller_authentication**](docs/AuthenticationApi.md#auth_controller_authentication) | **POST** /oauth/authentication | Token which is valid for the app
*AuthenticationApi* | [**auth_controller_authorize**](docs/AuthenticationApi.md#auth_controller_authorize) | **GET** /oauth/authorize | Add a new vehicle
*ChargingApi* | [**get_many_base_card_controller_card**](docs/ChargingApi.md#get_many_base_card_controller_card) | **GET** /v1/cards | Retrieve many Card
*ChargingApi* | [**get_many_base_chargepoint_controller_chargepoint**](docs/ChargingApi.md#get_many_base_chargepoint_controller_chargepoint) | **GET** /v1/chargepoints | Retrieve many Chargepoint
*ChargingApi* | [**get_many_base_operator_controller_operator**](docs/ChargingApi.md#get_many_base_operator_controller_operator) | **GET** /v1/operators | Retrieve many Operator
*ChargingApi* | [**get_one_base_operator_controller_operator**](docs/ChargingApi.md#get_one_base_operator_controller_operator) | **GET** /v1/operators/{id} | Retrieve one Operator
*PlatformApi* | [**carbon_controller_avg**](docs/PlatformApi.md#carbon_controller_avg) | **GET** /v1/carbons/{country}/avg | The avg co2 polution for country in timeframe
*PlatformApi* | [**carbon_controller_latest**](docs/PlatformApi.md#carbon_controller_latest) | **GET** /v1/carbons/{country}/latest | The latest co2 polution for country and production MW
*PlatformApi* | [**create_one_base_app_controller_app**](docs/PlatformApi.md#create_one_base_app_controller_app) | **POST** /v1/users/{userId}/apps | Create one App
*PlatformApi* | [**create_one_base_webhook_controller_webhook**](docs/PlatformApi.md#create_one_base_webhook_controller_webhook) | **POST** /v1/users/{userId}/apps/{appId}/webhooks | Create one Webhook
*PlatformApi* | [**delete_one_base_app_controller_app**](docs/PlatformApi.md#delete_one_base_app_controller_app) | **DELETE** /v1/users/{userId}/apps/{id} | Delete one App
*PlatformApi* | [**delete_one_base_webhook_controller_webhook**](docs/PlatformApi.md#delete_one_base_webhook_controller_webhook) | **DELETE** /v1/users/{userId}/apps/{appId}/webhooks/{id} | Delete one Webhook
*PlatformApi* | [**get_many_base_app_controller_app**](docs/PlatformApi.md#get_many_base_app_controller_app) | **GET** /v1/users/{userId}/apps | Retrieve many Apps
*PlatformApi* | [**get_many_base_user_controller_user**](docs/PlatformApi.md#get_many_base_user_controller_user) | **GET** /v1/users | Retrieve many User
*PlatformApi* | [**get_many_base_webhook_controller_webhook**](docs/PlatformApi.md#get_many_base_webhook_controller_webhook) | **GET** /v1/users/{userId}/apps/{appId}/webhooks | Retrieve many Webhook
*PlatformApi* | [**get_one_base_app_controller_app**](docs/PlatformApi.md#get_one_base_app_controller_app) | **GET** /v1/users/{userId}/apps/{id} | Retrieve one App
*PlatformApi* | [**update_one_base_app_controller_app**](docs/PlatformApi.md#update_one_base_app_controller_app) | **PATCH** /v1/users/{userId}/apps/{id} | Update one App
*PlatformApi* | [**update_one_base_webhook_controller_webhook**](docs/PlatformApi.md#update_one_base_webhook_controller_webhook) | **PATCH** /v1/users/{userId}/apps/{appId}/webhooks/{id} | Update one Webhook
*VehiclesApi* | [**delete_one_base_vehicle_controller_vehicle**](docs/VehiclesApi.md#delete_one_base_vehicle_controller_vehicle) | **DELETE** /v1/vehicles/{id} | Delete one Vehicle
*VehiclesApi* | [**get_many_base_charge_controller_charge**](docs/VehiclesApi.md#get_many_base_charge_controller_charge) | **GET** /v1/vehicles/{vehicleId}/charges | Retrieve many Charge
*VehiclesApi* | [**get_many_base_idle_controller_idle**](docs/VehiclesApi.md#get_many_base_idle_controller_idle) | **GET** /v1/vehicles/{vehicleId}/idles | Retrieve many Idle
*VehiclesApi* | [**get_many_base_record_controller_record**](docs/VehiclesApi.md#get_many_base_record_controller_record) | **GET** /v1/vehicles/{vehicleId}/records | Retrieve many Record
*VehiclesApi* | [**get_many_base_sleep_controller_sleep**](docs/VehiclesApi.md#get_many_base_sleep_controller_sleep) | **GET** /v1/vehicles/{vehicleId}/sleeps | Retrieve many Sleep
*VehiclesApi* | [**get_many_base_trip_controller_trip**](docs/VehiclesApi.md#get_many_base_trip_controller_trip) | **GET** /v1/vehicles/{vehicleId}/trips | Retrieve many Trip
*VehiclesApi* | [**get_many_base_vehicle_controller_vehicle**](docs/VehiclesApi.md#get_many_base_vehicle_controller_vehicle) | **GET** /v1/vehicles | Retrieve many Vehicle
*VehiclesApi* | [**trip_controller_avg**](docs/VehiclesApi.md#trip_controller_avg) | **GET** /v1/vehicles/{vehicleId}/trips/{year}/{month}/avg | Get avg consumption and distance for the month
*VehiclesApi* | [**vehicle_controller_battery**](docs/VehiclesApi.md#vehicle_controller_battery) | **GET** /v1/vehicles/{vehicleId}/battery | Read battery&#39;s state of charge
*VehiclesApi* | [**vehicle_controller_bulk**](docs/VehiclesApi.md#vehicle_controller_bulk) | **GET** /v1/vehicles/{vehicleId}/bulk | Read bulk data based on vehilce scope
*VehiclesApi* | [**vehicle_controller_charge**](docs/VehiclesApi.md#vehicle_controller_charge) | **GET** /v1/vehicles/{vehicleId}/charge | Know whether vehicle is charging
*VehiclesApi* | [**vehicle_controller_charge_start**](docs/VehiclesApi.md#vehicle_controller_charge_start) | **POST** /v1/vehicles/{vehicleId}/charge_start | Start Charging
*VehiclesApi* | [**vehicle_controller_charge_stop**](docs/VehiclesApi.md#vehicle_controller_charge_stop) | **POST** /v1/vehicles/{vehicleId}/charge_stop | Stop Charging
*VehiclesApi* | [**vehicle_controller_location**](docs/VehiclesApi.md#vehicle_controller_location) | **GET** /v1/vehicles/{vehicleId}/location | Returns the last known location of the vehicle in geographic coordinates.
*VehiclesApi* | [**vehicle_controller_odometer**](docs/VehiclesApi.md#vehicle_controller_odometer) | **GET** /v1/vehicles/{vehicleId}/odometer | Get odometer from the vehicle
*VehiclesApi* | [**vehicle_controller_vin**](docs/VehiclesApi.md#vehicle_controller_vin) | **GET** /v1/vehicles/{vehicleId}/vin | Get vin from the vehicle
*VehiclesApi* | [**vehicle_controller_wake_up**](docs/VehiclesApi.md#vehicle_controller_wake_up) | **POST** /v1/vehicles/{vehicleId}/wake_up | Wake Up Car


## Documentation For Models

 - [App](docs/App.md)
 - [Card](docs/Card.md)
 - [Charge](docs/Charge.md)
 - [Chargepoint](docs/Chargepoint.md)
 - [GetManyAppResponseDto](docs/GetManyAppResponseDto.md)
 - [GetManyCardResponseDto](docs/GetManyCardResponseDto.md)
 - [GetManyChargeResponseDto](docs/GetManyChargeResponseDto.md)
 - [GetManyChargepointResponseDto](docs/GetManyChargepointResponseDto.md)
 - [GetManyIdleResponseDto](docs/GetManyIdleResponseDto.md)
 - [GetManyOperatorResponseDto](docs/GetManyOperatorResponseDto.md)
 - [GetManyRecordResponseDto](docs/GetManyRecordResponseDto.md)
 - [GetManySleepResponseDto](docs/GetManySleepResponseDto.md)
 - [GetManyTripResponseDto](docs/GetManyTripResponseDto.md)
 - [GetManyUserResponseDto](docs/GetManyUserResponseDto.md)
 - [GetManyVehicleResponseDto](docs/GetManyVehicleResponseDto.md)
 - [GetManyWebhookResponseDto](docs/GetManyWebhookResponseDto.md)
 - [Idle](docs/Idle.md)
 - [InlineObject](docs/InlineObject.md)
 - [InlineResponse200](docs/InlineResponse200.md)
 - [InlineResponse2001](docs/InlineResponse2001.md)
 - [InlineResponse2002](docs/InlineResponse2002.md)
 - [InlineResponse2003](docs/InlineResponse2003.md)
 - [InlineResponse2004](docs/InlineResponse2004.md)
 - [InlineResponse2005](docs/InlineResponse2005.md)
 - [InlineResponse2006](docs/InlineResponse2006.md)
 - [InlineResponse2007](docs/InlineResponse2007.md)
 - [InlineResponse2008](docs/InlineResponse2008.md)
 - [InlineResponse2009](docs/InlineResponse2009.md)
 - [Operator](docs/Operator.md)
 - [Record](docs/Record.md)
 - [Sleep](docs/Sleep.md)
 - [Trip](docs/Trip.md)
 - [User](docs/User.md)
 - [Vehicle](docs/Vehicle.md)
 - [Webhook](docs/Webhook.md)


## Documentation For Authorization


## bearer

- **Type**: Bearer authentication (JWT)
# tronity_platform_client.AuthenticationApi

All URIs are relative to *https://api.eu.tronity.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**auth_controller_authentication**](AuthenticationApi.md#auth_controller_authentication) | **POST** /oauth/authentication | Token which is valid for the app
[**auth_controller_authorize**](AuthenticationApi.md#auth_controller_authorize) | **GET** /oauth/authorize | Add a new vehicle


# **auth_controller_authentication**
> InlineResponse200 auth_controller_authentication(inline_object)

Token which is valid for the app

### Example

```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)


# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.AuthenticationApi(api_client)
    inline_object = tronity_platform_client.InlineObject() # InlineObject | 

    try:
        # Token which is valid for the app
        api_response = api_instance.auth_controller_authentication(inline_object)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AuthenticationApi->auth_controller_authentication: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object** | [**InlineObject**](InlineObject.md)|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**401** | The provided client_id or client_secret were incorrect. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **auth_controller_authorize**
> auth_controller_authorize(scopes, redirect_uri, client_id, state=state, makes=makes, make=make, tronity=tronity, tronity_only=tronity_only)

Add a new vehicle

We are compatible with the following manufaktur: BMW, MINI, Jaguar, Audi, Volkswagen, Skoda, Seat, Kia, Hyundai, Renault, Nissan, Opel, Peugeot, Mercedes and Tesla.

### Example

```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)


# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.AuthenticationApi(api_client)
    scopes = 'scopes_example' # str | A space-separated list of permissions that your application is requesting access to. Following scopes are available: read_vin, read_vehicle_info, read_odometer, read_trips, read_charges, read_sleeps, read_idleas, read_charge, read_battery, read_location, write_trips, write_charges, write_sleeps, write_idles, write_charge_start_stop and write_wake_up
redirect_uri = 'redirect_uri_example' # str | The URI a user will be redirected to after authorization. This value must match one of the redirect URIs set in the application. We include in the redirect url the code and the state
client_id = 'client_id_example' # str | The application’s unique identifier. This value can you get in your application.
state = 'state_example' # str | If the redirect to Connect contains a state parameter, that parameter will be returned here. (optional)
makes = 'makes_example' # str | Show only this manufaktur which you want to support (optional)
make = 'make_example' # str | An optional parameter that allows users to bypass the car brand selection screen. (optional)
tronity = 'tronity_example' # str | Show tronity login and also possible to login with manufaktur (optional)
tronity_only = 'tronity_only_example' # str | Show only tronity login to add an vehilce (optional)

    try:
        # Add a new vehicle
        api_instance.auth_controller_authorize(scopes, redirect_uri, client_id, state=state, makes=makes, make=make, tronity=tronity, tronity_only=tronity_only)
    except ApiException as e:
        print("Exception when calling AuthenticationApi->auth_controller_authorize: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scopes** | **str**| A space-separated list of permissions that your application is requesting access to. Following scopes are available: read_vin, read_vehicle_info, read_odometer, read_trips, read_charges, read_sleeps, read_idleas, read_charge, read_battery, read_location, write_trips, write_charges, write_sleeps, write_idles, write_charge_start_stop and write_wake_up | 
 **redirect_uri** | **str**| The URI a user will be redirected to after authorization. This value must match one of the redirect URIs set in the application. We include in the redirect url the code and the state | 
 **client_id** | **str**| The application’s unique identifier. This value can you get in your application. | 
 **state** | **str**| If the redirect to Connect contains a state parameter, that parameter will be returned here. | [optional] 
 **makes** | **str**| Show only this manufaktur which you want to support | [optional] 
 **make** | **str**| An optional parameter that allows users to bypass the car brand selection screen. | [optional] 
 **tronity** | **str**| Show tronity login and also possible to login with manufaktur | [optional] 
 **tronity_only** | **str**| Show only tronity login to add an vehilce | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**302** | Redirect from Tronity Connect with code and state when success. You can get also following error code \&quot;subscription_limit_reached\&quot; or \&quot;no_vehicles\&quot;. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


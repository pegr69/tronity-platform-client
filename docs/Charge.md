# Charge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**start_time** | **float** |  | 
**start_level** | **float** |  | 
**end_time** | **float** |  | 
**end_level** | **float** |  | 
**odometer** | **float** |  | 
**comment** | **str** |  | 
**tags** | **list[str]** |  | 
**type** | **str** |  | 
**k_wh** | **float** |  | 
**batteryk_wh** | **float** |  | 
**max** | **float** |  | 
**range** | **float** |  | 
**ac** | **bool** |  | 
**cost** | **float** |  | 
**location** | [**object**](.md) |  | 
**latitude** | **str** |  | 
**longitude** | **str** |  | 
**items** | [**list[Record]**](Record.md) |  | 
**created_at** | **datetime** |  | 
**updated_at** | **datetime** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



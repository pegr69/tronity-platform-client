# Chargepoint

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**reference_id** | **str** |  | 
**name** | **str** |  | 
**address** | **str** |  | 
**city** | **str** |  | 
**postal_code** | **str** |  | 
**country** | **str** |  | 
**capabilities** | **list[str]** |  | 
**evses** | [**object**](.md) |  | 
**location** | [**object**](.md) |  | 
**operator** | [**Operator**](Operator.md) |  | 
**created_at** | **datetime** |  | 
**updated_at** | **datetime** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



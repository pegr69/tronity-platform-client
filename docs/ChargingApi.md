# tronity_platform_client.ChargingApi

All URIs are relative to *https://api.eu.tronity.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_many_base_card_controller_card**](ChargingApi.md#get_many_base_card_controller_card) | **GET** /v1/cards | Retrieve many Card
[**get_many_base_chargepoint_controller_chargepoint**](ChargingApi.md#get_many_base_chargepoint_controller_chargepoint) | **GET** /v1/chargepoints | Retrieve many Chargepoint
[**get_many_base_operator_controller_operator**](ChargingApi.md#get_many_base_operator_controller_operator) | **GET** /v1/operators | Retrieve many Operator
[**get_one_base_operator_controller_operator**](ChargingApi.md#get_one_base_operator_controller_operator) | **GET** /v1/operators/{id} | Retrieve one Operator


# **get_many_base_card_controller_card**
> OneOfGetManyCardResponseDtoarray get_many_base_card_controller_card(fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)

Retrieve many Card

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.ChargingApi(api_client)
    fields = ['field1,field2'] # list[str] |  (optional)
s = '{\"name\": \"Michael\"}' # str |  (optional)
sort = ['name,ASC&sort=id,DESC'] # list[str] |  (optional)
join = ['relation||field1,field2'] # list[str] |  (optional)
limit = 56 # int |  (optional)
offset = 56 # int |  (optional)

    try:
        # Retrieve many Card
        api_response = api_instance.get_many_base_card_controller_card(fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ChargingApi->get_many_base_card_controller_card: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [**list[str]**](str.md)|  | [optional] 
 **s** | **str**|  | [optional] 
 **sort** | [**list[str]**](str.md)|  | [optional] 
 **join** | [**list[str]**](str.md)|  | [optional] 
 **limit** | **int**|  | [optional] 
 **offset** | **int**|  | [optional] 

### Return type

[**OneOfGetManyCardResponseDtoarray**](OneOfGetManyCardResponseDtoarray.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get many base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_many_base_chargepoint_controller_chargepoint**
> OneOfGetManyChargepointResponseDtoarray get_many_base_chargepoint_controller_chargepoint(fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)

Retrieve many Chargepoint

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.ChargingApi(api_client)
    fields = ['field1,field2'] # list[str] |  (optional)
s = '{\"name\": \"Michael\"}' # str |  (optional)
sort = ['name,ASC&sort=id,DESC'] # list[str] |  (optional)
join = ['relation||field1,field2'] # list[str] |  (optional)
limit = 56 # int |  (optional)
offset = 56 # int |  (optional)

    try:
        # Retrieve many Chargepoint
        api_response = api_instance.get_many_base_chargepoint_controller_chargepoint(fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ChargingApi->get_many_base_chargepoint_controller_chargepoint: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [**list[str]**](str.md)|  | [optional] 
 **s** | **str**|  | [optional] 
 **sort** | [**list[str]**](str.md)|  | [optional] 
 **join** | [**list[str]**](str.md)|  | [optional] 
 **limit** | **int**|  | [optional] 
 **offset** | **int**|  | [optional] 

### Return type

[**OneOfGetManyChargepointResponseDtoarray**](OneOfGetManyChargepointResponseDtoarray.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get many base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_many_base_operator_controller_operator**
> OneOfGetManyOperatorResponseDtoarray get_many_base_operator_controller_operator(fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)

Retrieve many Operator

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.ChargingApi(api_client)
    fields = ['field1,field2'] # list[str] |  (optional)
s = '{\"name\": \"Michael\"}' # str |  (optional)
sort = ['name,ASC&sort=id,DESC'] # list[str] |  (optional)
join = ['relation||field1,field2'] # list[str] |  (optional)
limit = 56 # int |  (optional)
offset = 56 # int |  (optional)

    try:
        # Retrieve many Operator
        api_response = api_instance.get_many_base_operator_controller_operator(fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ChargingApi->get_many_base_operator_controller_operator: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [**list[str]**](str.md)|  | [optional] 
 **s** | **str**|  | [optional] 
 **sort** | [**list[str]**](str.md)|  | [optional] 
 **join** | [**list[str]**](str.md)|  | [optional] 
 **limit** | **int**|  | [optional] 
 **offset** | **int**|  | [optional] 

### Return type

[**OneOfGetManyOperatorResponseDtoarray**](OneOfGetManyOperatorResponseDtoarray.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get many base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_one_base_operator_controller_operator**
> Operator get_one_base_operator_controller_operator(id)

Retrieve one Operator

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.ChargingApi(api_client)
    id = 'id_example' # str | 

    try:
        # Retrieve one Operator
        api_response = api_instance.get_one_base_operator_controller_operator(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ChargingApi->get_one_base_operator_controller_operator: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

[**Operator**](Operator.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get one base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


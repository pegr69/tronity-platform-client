# tronity_platform_client.PlatformApi

All URIs are relative to *https://api.eu.tronity.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**carbon_controller_avg**](PlatformApi.md#carbon_controller_avg) | **GET** /v1/carbons/{country}/avg | The avg co2 polution for country in timeframe
[**carbon_controller_latest**](PlatformApi.md#carbon_controller_latest) | **GET** /v1/carbons/{country}/latest | The latest co2 polution for country and production MW
[**create_one_base_app_controller_app**](PlatformApi.md#create_one_base_app_controller_app) | **POST** /v1/users/{userId}/apps | Create one App
[**create_one_base_webhook_controller_webhook**](PlatformApi.md#create_one_base_webhook_controller_webhook) | **POST** /v1/users/{userId}/apps/{appId}/webhooks | Create one Webhook
[**delete_one_base_app_controller_app**](PlatformApi.md#delete_one_base_app_controller_app) | **DELETE** /v1/users/{userId}/apps/{id} | Delete one App
[**delete_one_base_webhook_controller_webhook**](PlatformApi.md#delete_one_base_webhook_controller_webhook) | **DELETE** /v1/users/{userId}/apps/{appId}/webhooks/{id} | Delete one Webhook
[**get_many_base_app_controller_app**](PlatformApi.md#get_many_base_app_controller_app) | **GET** /v1/users/{userId}/apps | Retrieve many Apps
[**get_many_base_user_controller_user**](PlatformApi.md#get_many_base_user_controller_user) | **GET** /v1/users | Retrieve many User
[**get_many_base_webhook_controller_webhook**](PlatformApi.md#get_many_base_webhook_controller_webhook) | **GET** /v1/users/{userId}/apps/{appId}/webhooks | Retrieve many Webhook
[**get_one_base_app_controller_app**](PlatformApi.md#get_one_base_app_controller_app) | **GET** /v1/users/{userId}/apps/{id} | Retrieve one App
[**update_one_base_app_controller_app**](PlatformApi.md#update_one_base_app_controller_app) | **PATCH** /v1/users/{userId}/apps/{id} | Update one App
[**update_one_base_webhook_controller_webhook**](PlatformApi.md#update_one_base_webhook_controller_webhook) | **PATCH** /v1/users/{userId}/apps/{appId}/webhooks/{id} | Update one Webhook


# **carbon_controller_avg**
> InlineResponse2009 carbon_controller_avg(country, start, end)

The avg co2 polution for country in timeframe

### Example

```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)


# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.PlatformApi(api_client)
    country = 'country_example' # str | The country
start = '2020-11-01T16:00Z' # str | The start time in UTC
end = '2020-11-01T16:00Z' # str | The end time in UTC

    try:
        # The avg co2 polution for country in timeframe
        api_response = api_instance.carbon_controller_avg(country, start, end)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlatformApi->carbon_controller_avg: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **str**| The country | 
 **start** | **str**| The start time in UTC | 
 **end** | **str**| The end time in UTC | 

### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**401** | The provided token were incorrect. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **carbon_controller_latest**
> InlineResponse2008 carbon_controller_latest(country)

The latest co2 polution for country and production MW

### Example

```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)


# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.PlatformApi(api_client)
    country = 'country_example' # str | The country

    try:
        # The latest co2 polution for country and production MW
        api_response = api_instance.carbon_controller_latest(country)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlatformApi->carbon_controller_latest: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **str**| The country | 

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**401** | The provided token were incorrect. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_one_base_app_controller_app**
> App create_one_base_app_controller_app(user_id, app)

Create one App

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.PlatformApi(api_client)
    user_id = 'user_id_example' # str | 
app = tronity_platform_client.App() # App | 

    try:
        # Create one App
        api_response = api_instance.create_one_base_app_controller_app(user_id, app)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlatformApi->create_one_base_app_controller_app: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**|  | 
 **app** | [**App**](App.md)|  | 

### Return type

[**App**](App.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Get create one base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_one_base_webhook_controller_webhook**
> Webhook create_one_base_webhook_controller_webhook(user_id, app_id, webhook)

Create one Webhook

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.PlatformApi(api_client)
    user_id = 'user_id_example' # str | 
app_id = 'app_id_example' # str | 
webhook = tronity_platform_client.Webhook() # Webhook | 

    try:
        # Create one Webhook
        api_response = api_instance.create_one_base_webhook_controller_webhook(user_id, app_id, webhook)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlatformApi->create_one_base_webhook_controller_webhook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**|  | 
 **app_id** | **str**|  | 
 **webhook** | [**Webhook**](Webhook.md)|  | 

### Return type

[**Webhook**](Webhook.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Get create one base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_one_base_app_controller_app**
> delete_one_base_app_controller_app(id, user_id)

Delete one App

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.PlatformApi(api_client)
    id = 'id_example' # str | 
user_id = 'user_id_example' # str | 

    try:
        # Delete one App
        api_instance.delete_one_base_app_controller_app(id, user_id)
    except ApiException as e:
        print("Exception when calling PlatformApi->delete_one_base_app_controller_app: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **user_id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Delete one base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_one_base_webhook_controller_webhook**
> delete_one_base_webhook_controller_webhook(id, user_id, app_id)

Delete one Webhook

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.PlatformApi(api_client)
    id = 'id_example' # str | 
user_id = 'user_id_example' # str | 
app_id = 'app_id_example' # str | 

    try:
        # Delete one Webhook
        api_instance.delete_one_base_webhook_controller_webhook(id, user_id, app_id)
    except ApiException as e:
        print("Exception when calling PlatformApi->delete_one_base_webhook_controller_webhook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **user_id** | **str**|  | 
 **app_id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Delete one base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_many_base_app_controller_app**
> OneOfGetManyAppResponseDtoarray get_many_base_app_controller_app(user_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)

Retrieve many Apps

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.PlatformApi(api_client)
    user_id = 'user_id_example' # str | 
fields = ['field1,field2'] # list[str] |  (optional)
s = '{\"name\": \"Michael\"}' # str |  (optional)
sort = ['name,ASC&sort=id,DESC'] # list[str] |  (optional)
join = ['relation||field1,field2'] # list[str] |  (optional)
limit = 56 # int |  (optional)
offset = 56 # int |  (optional)

    try:
        # Retrieve many Apps
        api_response = api_instance.get_many_base_app_controller_app(user_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlatformApi->get_many_base_app_controller_app: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**|  | 
 **fields** | [**list[str]**](str.md)|  | [optional] 
 **s** | **str**|  | [optional] 
 **sort** | [**list[str]**](str.md)|  | [optional] 
 **join** | [**list[str]**](str.md)|  | [optional] 
 **limit** | **int**|  | [optional] 
 **offset** | **int**|  | [optional] 

### Return type

[**OneOfGetManyAppResponseDtoarray**](OneOfGetManyAppResponseDtoarray.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get many base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_many_base_user_controller_user**
> GetManyUserResponseDto get_many_base_user_controller_user(fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)

Retrieve many User

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.PlatformApi(api_client)
    fields = ['field1,field2'] # list[str] |  (optional)
s = '{\"name\": \"Michael\"}' # str |  (optional)
sort = ['name,ASC&sort=id,DESC'] # list[str] |  (optional)
join = ['relation||field1,field2'] # list[str] |  (optional)
limit = 56 # int |  (optional)
offset = 56 # int |  (optional)

    try:
        # Retrieve many User
        api_response = api_instance.get_many_base_user_controller_user(fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlatformApi->get_many_base_user_controller_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [**list[str]**](str.md)|  | [optional] 
 **s** | **str**|  | [optional] 
 **sort** | [**list[str]**](str.md)|  | [optional] 
 **join** | [**list[str]**](str.md)|  | [optional] 
 **limit** | **int**|  | [optional] 
 **offset** | **int**|  | [optional] 

### Return type

[**GetManyUserResponseDto**](GetManyUserResponseDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get paginated response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_many_base_webhook_controller_webhook**
> OneOfGetManyWebhookResponseDtoarray get_many_base_webhook_controller_webhook(user_id, app_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)

Retrieve many Webhook

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.PlatformApi(api_client)
    user_id = 'user_id_example' # str | 
app_id = 'app_id_example' # str | 
fields = ['field1,field2'] # list[str] |  (optional)
s = '{\"name\": \"Michael\"}' # str |  (optional)
sort = ['name,ASC&sort=id,DESC'] # list[str] |  (optional)
join = ['relation||field1,field2'] # list[str] |  (optional)
limit = 56 # int |  (optional)
offset = 56 # int |  (optional)

    try:
        # Retrieve many Webhook
        api_response = api_instance.get_many_base_webhook_controller_webhook(user_id, app_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlatformApi->get_many_base_webhook_controller_webhook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**|  | 
 **app_id** | **str**|  | 
 **fields** | [**list[str]**](str.md)|  | [optional] 
 **s** | **str**|  | [optional] 
 **sort** | [**list[str]**](str.md)|  | [optional] 
 **join** | [**list[str]**](str.md)|  | [optional] 
 **limit** | **int**|  | [optional] 
 **offset** | **int**|  | [optional] 

### Return type

[**OneOfGetManyWebhookResponseDtoarray**](OneOfGetManyWebhookResponseDtoarray.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get many base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_one_base_app_controller_app**
> App get_one_base_app_controller_app(id, user_id)

Retrieve one App

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.PlatformApi(api_client)
    id = 'id_example' # str | 
user_id = 'user_id_example' # str | 

    try:
        # Retrieve one App
        api_response = api_instance.get_one_base_app_controller_app(id, user_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlatformApi->get_one_base_app_controller_app: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **user_id** | **str**|  | 

### Return type

[**App**](App.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get one base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_one_base_app_controller_app**
> App update_one_base_app_controller_app(id, user_id, app)

Update one App

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.PlatformApi(api_client)
    id = 'id_example' # str | 
user_id = 'user_id_example' # str | 
app = tronity_platform_client.App() # App | 

    try:
        # Update one App
        api_response = api_instance.update_one_base_app_controller_app(id, user_id, app)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlatformApi->update_one_base_app_controller_app: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **user_id** | **str**|  | 
 **app** | [**App**](App.md)|  | 

### Return type

[**App**](App.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_one_base_webhook_controller_webhook**
> Webhook update_one_base_webhook_controller_webhook(id, user_id, app_id, webhook)

Update one Webhook

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.PlatformApi(api_client)
    id = 'id_example' # str | 
user_id = 'user_id_example' # str | 
app_id = 'app_id_example' # str | 
webhook = tronity_platform_client.Webhook() # Webhook | 

    try:
        # Update one Webhook
        api_response = api_instance.update_one_base_webhook_controller_webhook(id, user_id, app_id, webhook)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlatformApi->update_one_base_webhook_controller_webhook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **user_id** | **str**|  | 
 **app_id** | **str**|  | 
 **webhook** | [**Webhook**](Webhook.md)|  | 

### Return type

[**Webhook**](Webhook.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# Record

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** |  | 
**level** | **float** |  | 
**usable_level** | **float** |  | 
**range** | **float** |  | 
**speed** | **float** |  | 
**power** | **float** |  | 
**charger_power** | **float** |  | 
**phases** | **float** |  | 
**voltage** | **float** |  | 
**current** | **float** |  | 
**energy_added** | **float** |  | 
**miles_added** | **float** |  | 
**soc_max** | **float** |  | 
**consumption** | **float** |  | 
**out_temp** | **float** |  | 
**in_temp** | **float** |  | 
**pass_temp** | **float** |  | 
**limit_soc** | **float** |  | 
**elevation** | **float** |  | 
**fan** | **float** |  | 
**climate** | **bool** |  | 
**seat_heating** | **bool** |  | 
**preconditioning** | **bool** |  | 
**charging** | **str** |  | 
**driving** | **bool** |  | 
**wakeup** | **bool** |  | 
**supercharger** | **bool** |  | 
**door_locked** | **bool** |  | 
**sentry_mode** | **bool** |  | 
**windows_locked** | **bool** |  | 
**odometer** | **float** |  | 
**vehicle_status_time** | **float** |  | 
**vehicle_location_time** | **float** |  | 
**trip_current_id** | **float** |  | 
**trip_travel_time** | **float** |  | 
**trip_timestamp** | **float** |  | 
**latitude** | **str** |  | 
**longitude** | **str** |  | 
**software_update** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# Trip

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**company** | **str** |  | 
**name** | **str** |  | 
**reason** | **str** |  | 
**comment** | **str** |  | 
**start_address** | **str** |  | 
**start_location** | [**object**](.md) |  | 
**start_time** | **float** |  | 
**start_level** | **float** |  | 
**end_address** | **str** |  | 
**end_location** | [**object**](.md) |  | 
**end_time** | **float** |  | 
**end_level** | **float** |  | 
**tags** | **list[str]** |  | 
**odometer** | **float** |  | 
**distance** | **float** |  | 
**usedk_wh** | **float** |  | 
**avg_speed** | **float** |  | 
**avg_out_temp** | **float** |  | 
**avg_in_temp** | **float** |  | 
**avg_consumption** | **float** |  | 
**cost** | **float** |  | 
**items** | [**list[Record]**](Record.md) |  | 
**created_at** | **datetime** |  | 
**updated_at** | **datetime** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



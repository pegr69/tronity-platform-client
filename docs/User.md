# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**company** | **str** |  | 
**first_name** | **str** |  | 
**last_name** | **str** |  | 
**street** | **str** |  | 
**zip_code** | **str** |  | 
**city** | **str** |  | 
**country** | **str** |  | 
**email** | **str** |  | 
**password** | **str** |  | 
**role** | **str** |  | 
**scopes** | **str** |  | 
**package** | **float** |  | 
**created_at** | **datetime** |  | 
**updated_at** | **datetime** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



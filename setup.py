import os
from setuptools import setup, find_packages

if os.environ.get('CI_COMMIT_TAG'):
    version = os.environ['CI_COMMIT_TAG']
elif os.environ.get('CI_JOB_ID'):
    version = os.environ['CI_JOB_ID']
else:
    version = os.environ['DEV']

with open("README.md", "r") as fh:
    long_description = fh.read()

REQUIRES = ["urllib3 >= 1.15", "six >= 1.10", "certifi", "python-dateutil"]

setup(
    name='tronity-platform-client',
    version=version,
    author='Dominik Fillinger',
    author_email='dev@fi-do.io',
    description='Python package to easily interact with Tronity Platform API',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/tronity/tronity-platform-client',
    packages=find_packages(exclude=["test", "tests"]),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        "License :: OSI Approved :: MIT License",
    ],
    python_requires='>=3.6',
    install_requires=REQUIRES,
)

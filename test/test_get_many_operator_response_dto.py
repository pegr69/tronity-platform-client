# coding: utf-8

"""
    Tronity Platform API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import tronity_platform_client
from tronity_platform_client.models.get_many_operator_response_dto import GetManyOperatorResponseDto  # noqa: E501
from tronity_platform_client.rest import ApiException

class TestGetManyOperatorResponseDto(unittest.TestCase):
    """GetManyOperatorResponseDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test GetManyOperatorResponseDto
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # model = tronity_platform_client.models.get_many_operator_response_dto.GetManyOperatorResponseDto()  # noqa: E501
        if include_optional :
            return GetManyOperatorResponseDto(
                data = [
                    tronity_platform_client.models.operator.Operator(
                        id = '0', 
                        code = '0', 
                        country = '0', 
                        name = '0', 
                        chargepoints = [
                            '0'
                            ], 
                        created_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                        updated_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), )
                    ], 
                count = 1.337, 
                total = 1.337, 
                page = 1.337, 
                page_count = 1.337
            )
        else :
            return GetManyOperatorResponseDto(
                data = [
                    tronity_platform_client.models.operator.Operator(
                        id = '0', 
                        code = '0', 
                        country = '0', 
                        name = '0', 
                        chargepoints = [
                            '0'
                            ], 
                        created_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                        updated_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), )
                    ],
                count = 1.337,
                total = 1.337,
                page = 1.337,
                page_count = 1.337,
        )

    def testGetManyOperatorResponseDto(self):
        """Test GetManyOperatorResponseDto"""
        inst_req_only = self.make_instance(include_optional=False)
        inst_req_and_optional = self.make_instance(include_optional=True)


if __name__ == '__main__':
    unittest.main()
